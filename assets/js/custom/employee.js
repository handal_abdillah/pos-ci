const employee = {
	initial: function () {
		employee.sendAjax($('#selectOrganization').val());
		$('#selectOrganization').on('change', function () {
			employee.sendAjax($('#selectOrganization').val());
		});
	},
	sendAjax: function (id) {
		object.data.idOrganization = id;
		employee.insertToSelect(employee.sendPost(object));
	},
	showData: function (obj) {
		return obj;
	},
	insertToSelect: function (data) {
		$('#selectPosition').empty();
		$.each(data, function (key, value) {
			$('#selectPosition')
				.append($("<option></option>")
					.attr("value", value.id)
					.text(value.position_name));
		});
	},
	sendPost: function (obj) {
		var result;
		$.ajax({
			url: obj.baseurl + obj.url + '/' + obj.function,
			type: obj.method,
			dataType: "JSON",
			data: obj.data,
			async: false,
			success: function (data) {
				result = data;
			}
		});
		return result;
	}
}

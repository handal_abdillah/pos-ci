<?php defined('BASEPATH') OR exit('No direct script access allowed');  

class Employee_model extends CI_Model {
  var $table = "employee";
  var $table_position = "position";
  var $table_organization = "organization";

  public function __construct(){
    parent::__construct();
  }

  public function getPosition(){
    $post = $this->input->post();
    $id = $post['idOrganization'];
    return $this->db->select('*')->from($this->table_position)->where('id_organization',$id)->get()->result_array();
  }

  public function getOrganization(){
    return $this->db->get($this->table_organization);
  }

  public function insertEmployee(){
    $this->form_validation->set_rules('employeeName', 'Employee Name', 'trim|required|min_length[3]');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('password_confirm', 'Confirm Password', 'required|matches[password]');

    if($this->form_validation->run() == FALSE){
      $error=$this->form_validation->error_array();
      $this->session->set_flashdata('error', $error);
      redirect('employee', 'refresh');
    }

    $post = $this->input->post();
    $data['employee_name'] = $post['employeeName'];
    $data['id_organization'] = $post['employeeOrganization'];
    $data['id_position'] = $post['employeePosition'];
    $data['local_pass'] = password_hash($post['password_confirm'], PASSWORD_BCRYPT);
    
    $this->db->insert($this->table, $data);
    if($this->db->affected_rows() > 0){
      $this->session->set_flashdata('status', 'insert success');
    }else{
      $this->session->set_flashdata('status', 'insert failed');
    }
    redirect('employee', 'refresh');
  }

}
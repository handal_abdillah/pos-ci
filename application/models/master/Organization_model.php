<?php defined('BASEPATH') OR exit('No direct script access allowed');  

class Organization_model extends CI_Model {

	private $table;
	private $pk;

  public function __construct(){
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');

    $this->table = 'organization';
    $this->pk    = 'id';
  }
  public function select_all(){
  	$this->db->select('*');
  	$this->db->from($this->table);

    return $this->db->get();
  }

}

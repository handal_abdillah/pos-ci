<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();

	}

	public function index(){
		$this->display_page("home/blank");
	}


	/**
	 * load view function for static header & layout
	 * @param  view $main 
	 * @param  array $data 
	 * @return        
	 */
	private function display_page($main,$data = NULL){
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view($main,$data);
		$this->load->view('layout/footer');
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organization extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('master/organization_model');
	}

	public function index(){

		$this->display_page("master/organization/index");
	}

	/**
	 * load view function for static header & layout
	 * @param  view $main 
	 * @param  array $data 
	 * @return        
	 */
	private function display_page($main,$data = NULL){
		$this->load->view('layout/header');
		$this->load->view($main,$data);
		$this->load->view('layout/footer');
	}

}

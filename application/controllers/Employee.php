<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('master/employee_model');
	}

    /**
	 * Index of page employee
	 * @return view list employee 
	 */
	public function index(){
        $data['title']='Employee';
        $data['module']='employee';
        $data['organization']= $this->employee_model->getOrganization()->result_array();
        $this->display_page("master/employee/index",$data);
	}

	/**
	 * Add employee
	 * @return view add employee 
	 */
	public function add(){
        $data['title']='Employee';
        $data['module']='employee';
        $data['organization']= $this->employee_model->getOrganization()->result_array();
        $this->display_page("master/employee/add",$data);
	}
	
	public function insert(){
		$this->employee_model->insertEmployee();
	}
    
    /**
	 * Get Data Ajax Organization
	 * @param  array $data 
	 * @return        
	 */
    public function getAjaxPosition(){
		$data = $this->employee_model->getPosition();
		echo json_encode($data);
    }

	/**
	 * load view function for static header & layout
	 * @param  view $main 
	 * @param  array $data 
	 * @return        
	 */
	private function display_page($main,$data = NULL){
		$this->load->view('layout/header');
		$this->load->view($main,$data);
		$this->load->view('layout/footer');
	}

}
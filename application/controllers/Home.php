<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();

	}

	public function index(){
		// ini ranggi
		$this->display_page("home/blank");
	}

	public function add($data){
		echo $data;
	}

	/**
	 * load view function for static header & layout
	 * @param  view $main 
	 * @param  array $data 
	 * @return        
	 */
	private function display_page($main,$data = NULL){
		$this->load->view('layout/header');
		$this->load->view('layout/nav');
		$this->load->view($main,$data);
		$this->load->view('layout/footer');
	}

}
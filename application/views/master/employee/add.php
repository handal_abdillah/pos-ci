<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"><?php echo $title ;?></h1>
    <p class="mb-4">DataTables is a third party plugin that is used </p>

    <div class="card shadow mb-4 col-lg-6">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Add Employee</h6>
            <br>
            <h6 class="m-0 text-primary">
                <?php 
                    echo $this->session->flashdata('status');
                    $error = $this->session->flashdata('error');
                    if($error){
                        foreach ($error as $key => $value) {
                            echo $value;
                            echo '<br>';
                        }
                    }
                ?>
            </h6>
        </div>
        <div class="card-body">
            <form id="formEmployee" action="<?php echo base_url().$module; ?>/insert" method="POST">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                    value="<?php echo $this->security->get_csrf_hash(); ?>">

                <div class="col-sm-12 form-group">
                    <label for="exampleInputEmail1">Employee Name</label>
                    <input name="employeeName" class="form-control form-control-user" type="text"
                        placeholder="Employee Name">
                </div>
                <div class="col-sm-12 form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input name="password" class="form-control form-control-user" type="password"
                        placeholder="Passowrd">
                </div>
                <div class="col-sm-12 form-group">
                    <label for="exampleInputEmail1">Password Confirm</label>
                    <input name="password_confirm" class="form-control form-control-user" type="password"
                        placeholder="Password Confirm">
                </div>
                <div class="col-sm-12 form-group">
                    <label for="exampleFormControlSelect1">Select Organization</label>
                    <select class="form-control" name="employeeOrganization" id="selectOrganization">
                        <?php
                            foreach ($organization as $key => $value) {
                                echo '<option value="'.$value['id'].'">'.$value['organization_name'].'</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="col-sm-12 form-group">
                    <label for="exampleFormControlSelect1">Select Position</label>
                    <select class="form-control" name="employeePosition" id="selectPosition">
                        <?php
                        ?>
                    </select>
                </div>
                <div class="col-sm-4 mb-3 mb-sm-0 form-group">
                    <br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

</div>
<script>
var object = {
    baseurl: "<?php echo base_url(); ?>",
    url: "<?php echo $module; ?>",
    function: "getAjaxPosition",
    method: "POST",
    data: {
        <?php echo $this->security->get_csrf_token_name(); ?>: "<?php echo $this->security->get_csrf_hash(); ?>",
        idOrganization: null
    }
};
</script>
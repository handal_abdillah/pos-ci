<?php
class MY_Loader extends CI_Loader {
    public function template($page_name, $vars = array(), $return = FALSE){
        if($return):
            $content  = $this->view('layout/header', $vars, $return);
            $content .= $this->view($page_name, $vars, $return);
            $content .= $this->view('layout/footer', $vars, $return);

            return $content;
        else:
            $this->view('layout/header', $vars);
            $this->view($page_name, $vars);
            $this->view('layout/footer', $vars);
        endif;
    }
}